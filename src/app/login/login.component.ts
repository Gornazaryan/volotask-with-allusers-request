import { Component, OnInit, ElementRef} from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from "../_services/index";

import { User, Login_user} from "../_models/index";
import { UsersService } from "app/_services/users.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [],
})

export class LoginComponent implements OnInit {
  username:string="";
  password:string="";
  
  constructor(private auth:AuthService ,private router:Router, 
    private usersService: UsersService){
  }  

  login() {
    let loginObj: Login_user = {
      "email": this.username,
      "password": this.password,
    }
    this.usersService.login(loginObj)
      .subscribe(() => {
        this.router.navigate(['home'])
      } )
  }

  ngOnInit(){
    if(localStorage.token){
      this.router.navigate(['home'])
    }
  }
}
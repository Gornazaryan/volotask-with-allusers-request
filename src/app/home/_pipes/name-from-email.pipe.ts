import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameFromEmail'
})
export class NameFromEmailPipe implements PipeTransform {

  transform(value: any): any {
    return value.substring(0, value.lastIndexOf("@"))
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizeFirstLetters'
})
export class CapitalizeFirstLettersPipe implements PipeTransform {

  transform(value: any, args ? : any): any {
    if (value) {
      let orig = value
      for (let i in value) {
        if (value[i] == " ") {
          let second = value.slice(i)
          return value.charAt(0).toUpperCase() + value.slice(1, i) + " " + this.transform(second.slice(1))
        }
      }
      return value.charAt(0).toUpperCase() + value.slice(1)
    }
    return value;
  }

}

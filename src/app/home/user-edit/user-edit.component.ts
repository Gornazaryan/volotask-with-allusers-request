import { Component, OnInit } from '@angular/core';
import { UsersService } from "app/_services";
import { Router } from "@angular/router";
import { User } from "app/_models";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  curentUser:User={
    "id":null,
    "first_name":null,
    "last_name":null,
    "avatar":null
  }
  editName:string
  editLastName:string

  constructor(private usersService:UsersService, private router:Router) { }

  cancel() {
    this.router.navigate(["home/users"])
  }
  
  save() {
    let editUser = {
      "id": this.curentUser.id,
      "first_name": this.editName,
      "last_name": this.editLastName,
    }
    this.usersService.editUser(editUser).subscribe(
      () => {
        for (let i in this.usersService.users) {
          if (this.usersService.users[i].id == this.usersService.userEditedData["id"]) {
            this.usersService.users[i].first_name = this.usersService.userEditedData["first_name"];
            this.usersService.users[i].last_name = this.usersService.userEditedData["last_name"];
            this.router.navigate(["home/users"])
            break
          }
        }
      }
    )
  }
  
  ngOnInit() {
    if (this.usersService.curentUser) {
      this.curentUser = this.usersService.curentUser
    } else {
      this.cancel()
    }
  }
  
}

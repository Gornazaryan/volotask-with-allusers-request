import { Component, OnInit, ElementRef } from '@angular/core';
import { User } from "app/_models";
import { UsersService } from "app/_services";
import { Router } from "@angular/router";
import { UsersListComponent } from "app/home/users-list/users-list.component";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})

export class AddUserComponent implements OnInit {

  constructor(private usersService: UsersService,
    private userComponent: UsersListComponent, private el: ElementRef) {}
  
  id: number = null;
  firstName: string = null;
  lastName: string = null;
  avatar: string = "http://lorempixel.com/200/200/people/?"
  
  
  save() {
    let newUser = {
      "first_name": this.firstName,
      "last_name": this.lastName
    }
    this.usersService.addUser(newUser)
      .subscribe(
        () => {
          let user = new User(this.usersService.userAddedData["id"],
            this.usersService.userAddedData["first_name"], this.usersService.userAddedData["last_name"],
            this.avatar + this.usersService.userAddedData["id"])
          this.usersService.users.push(user)
          this.userComponent.updateAllDataFromService()
          this.userComponent.pagechanger(this.userComponent.curentpage)
          this.userComponent.createPageArray()
          this.reset()
        }
      )
  }
  
  reset() {
    this.el.nativeElement.getElementsByTagName("form")[0].reset()
  }
  
  ngOnInit() {}
  
}

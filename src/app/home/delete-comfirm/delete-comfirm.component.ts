import { Component, OnInit, Input } from '@angular/core';
import { UsersListComponent } from "app/home/users-list/users-list.component";
import { User } from "app/_models";
import { UsersService } from "app/_services";

@Component({
  selector: 'app-delete-comfirm',
  templateUrl: './delete-comfirm.component.html',
  styleUrls: ['./delete-comfirm.component.css']
})
export class DeleteComfirmComponent implements OnInit {
  
  constructor(private usersComponent: UsersListComponent, private usersService: UsersService) {}

  delete() {
    this.usersService.delUser(this.usersComponent.curentUser.id)
      .subscribe(() =>
        this.usersComponent.removeUser(this.usersComponent.curentUser.id)
      )
  }

  ngOnInit() {}

}
  
import { Component, OnInit, ElementRef } from '@angular/core';
import { UsersService, SortingService, AuthService } from "app/_services";
import { User } from "app/_models";
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})

export class UsersListComponent implements OnInit {

  users: User[]
  onEachPage: Array < User > ;
  data: any;
  pages: Number[];
  curentpage: number;
  curentUser: User;
  targetElement: any;

  constructor(private sort: SortingService, private authService: AuthService,
    private route: Router, private usersService: UsersService, private el: ElementRef) {}


  setUser(user) {
    this.curentUser = user
  }
  
  sortByAZ(byWhat) {
    let thead = this.el.nativeElement.getElementsByTagName("thead")
    for (let i of thead[0].children) {
      i.classList.remove("sorted")
    }
    this.targetElement.classList.add("hide")
    this.targetElement.classList.remove("show")
    this.targetElement.nextElementSibling.classList.add("show")
    this.targetElement.nextElementSibling.classList.remove("hide")
    this.targetElement.parentElement.classList.add("sorted");
    this.users = this.sort.sortAZ(this.users, byWhat)
    this.usersService.users = this.users
    this.createPageArray()
    this.pagechanger(this.curentpage)
  }
  
  sortByZA(byWhat) {
    let thead = this.el.nativeElement.getElementsByTagName("thead")
    for (let i of thead[0].children) {
      //console.dir(i)
      i.classList.remove("sorted")
    }
    this.targetElement.classList.add("hide")
    this.targetElement.classList.remove("show")
    this.targetElement.previousElementSibling.classList.add("show")
    this.targetElement.previousElementSibling.classList.remove("hide")
    this.targetElement.parentElement.classList.add("sorted");
    this.users = this.sort.sortZA(this.users, byWhat)
    this.usersService.users = this.users
    this.createPageArray()
    this.pagechanger(this.curentpage)
  }
  
  pagechanger(pageNumber) {
    this.onEachPage = []
    for (let i = (pageNumber - 1) * this.data.per_page; i < pageNumber * this.data.per_page && i < this.users.length; i++) {
      this.onEachPage.push(this.users[i])
    }
    this.curentpage = pageNumber;
    this.usersService.curentpage = this.curentpage
  
    let icons = this.el.nativeElement.getElementsByClassName("pageIcon")
    let curentIcon = icons[pageNumber - 1]
    if (curentIcon !== undefined) {
      for (let i of icons) {
        i.classList.remove("active")
      }
      curentIcon.className += " active";
    }
  }
  
  createPageArray() {
    this.pages = []
    for (let i = 1; i < (this.users.length / this.data.per_page) + 1; i++) {
      this.pages.push(i)
    }
    this.pages = this.pages
  }
  
  removeUser(id) {
    for (let i in this.users) {
      if (this.users[i].id == id) {
        this.users.splice(Number(i), 1)
        break
      }
    }
    for (let j in this.onEachPage) {
      if (this.onEachPage[j].id == id) {
        this.onEachPage.splice(Number(j), 1)
        break
      }
    }
    if (this.onEachPage.length % this.data.per_page == 0) {
      if (this.curentpage !== 1) {
        this.curentpage--
      }
    }
    this.createPageArray()
    this.pagechanger(this.curentpage)
  }
  
  edit(user) {
    this.curentUser = user
    this.usersService.curentUser = this.curentUser
    this.route.navigate(["home/edituser", user.id]);
  
  }
  
  updateAllDataFromService() {
    this.users = this.usersService.users
    this.curentUser = this.usersService.curentUser
    this.data = this.usersService.data
    this.curentpage = this.usersService.curentpage
  }
  
  ngOnInit(): void {
    if (!this.usersService.users) {
      this.usersService.getAllUsers()
        .subscribe(
          result => {
            this.updateAllDataFromService()
            this.createPageArray();
            this.pagechanger(this.curentpage);
          }
        )
    } else {
      this.updateAllDataFromService()
      this.createPageArray();
      this.pagechanger(this.curentpage);
    }
    document.addEventListener("mouseover", (event) => {
      this.targetElement = event.target
    }, )
  }
    
}

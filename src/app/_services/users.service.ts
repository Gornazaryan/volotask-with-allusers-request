import { Injectable } from '@angular/core';
import { AuthService } from "app/_services";
import { User, Login_user } from "app/_models";
import { Observable } from 'rxjs/Rx';
import { Http } from "@angular/http";
import { Response, Headers } from '@angular/http';

@Injectable()
export class UsersService {
  pageURL:string='https://reqres.in/api/users?page=';
  loginURL:string = "https://reqres.in/api/login/"
  userURL:string = "https://reqres.in/api/users/"
  users:User[]
  data;
  curentpage:number=1;
  curentUser:User;
  userAddedData:object;
  userEditedData:object;
  status:boolean=false;
  

  constructor(private authService: AuthService, private http: Http) {}
  
  getUsers(page): Observable < any > {
    return this.http.get(this.pageURL + page)
      .map((resp: Response) => {
        let _resp = resp.json()
        return _resp
      });
  }

  login(obj: Login_user) {
    this.status = true
    const body = JSON.stringify(obj);
    let headers = new Headers({
      'Content-Type': 'application/json;charset=utf-8'
    });

    return this.http.post(this.loginURL, body, {
        headers: headers
      })
      .map((resp: Response) => {
        this.authService.user["token"] = resp.json().token
        this.authService.user["email"] = obj.email
      })
      .catch((error: any) => {
        return Observable.throw(error)
      })
      .finally(() => {
        this.status = false
      })
  }

  getAllUsers() {
    this.status = true
    return Observable.forkJoin([this.getUsers(1), this.getUsers(2),
        this.getUsers(3), this.getUsers(4)
      ])
      .map(
        (result) => {
          this.data = result[0];
          this.users = result[0].data
          this.users = this.users.concat(result[1].data,
            result[2].data, result[3].data);
        }
      )
      .finally(() => this.status = false)
  }

  addUser(obj) {
    this.status = true
    const body = JSON.stringify(obj);
    let headers = new Headers({
      'Content-Type': 'application/json;charset=utf-8'
    });

    return this.http.post(this.userURL, body, {
        headers: headers
      })
      .map((resp: Response) => {
        this.userAddedData = resp.json()
      })
      .catch((error: any) => {
        return Observable.throw(error);
      })
      .finally(() => this.status = false)
  }

  delUser(id) {
    this.status = true
    return this.http.delete(this.userURL + id)
      .finally(() => this.status = false)
  }

  editUser(obj) {
    this.status = true
    const body = JSON.stringify(obj);
    let headers = new Headers({
      'Content-Type': 'application/json;charset=utf-8'
    });

    return this.http.put(this.userURL + obj.id, body, {
        headers: headers
      })
      .map((resp: Response) => {
        this.userEditedData = resp.json()
      })
      .catch((error: any) => {
        return Observable.throw(error);
      })
      .finally(() => this.status = false)
  }

}

import { Injectable } from '@angular/core';

@Injectable()
export class SortingService {

  constructor() { }

  sortAZ(arr, byWhat) {
    arr.sort(
      function(a, b) {
        if (a[byWhat] < b[byWhat])
          return -1;
        if (a[byWhat] > b[byWhat])
          return 1;
        return 0;
      })
    return arr
  }
  
  sortZA(arr, byWhat) {
    arr.sort(
      function(a, b) {
        if (a[byWhat] > b[byWhat])
          return -1;
        if (a[byWhat] < b[byWhat])
          return 1;
        return 0;
      })
    return arr
  }
}
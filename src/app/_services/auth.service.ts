import { Injectable } from '@angular/core';
import { User } from "app/_models";
import { Router } from "@angular/router";

@Injectable()
export class AuthService {

  user: Object = {
    email: String,
    token: String
  }
  
  constructor(private route: Router) {
    this.user = localStorage
  }
  
  logOut() {
    delete this.user["token"]
    delete this.user["email"]
    this.route.navigate(["login"])
  }

}
